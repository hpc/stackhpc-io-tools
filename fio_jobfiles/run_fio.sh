#!/bin/bash
set -ux


FIO_CMD=$(which fio 2>/dev/null)
if [ -z "${FIO_CMD}" ]; then
    echo "E: missing fio executable."
    exit 1
else
    [ ${DEBUG} -eq 1 ] && \
     echo "D: FIO_CMD: ${FIO_CMD}"
fi

## notes for CentOS-6/EPEL:
## - it has 2.0.13, while filename_format has been added in version 2.1
##   (cf. fio's de98bd30b02bd89a78059d162b2c8426e889703d) and json+ in
##   version 2.2.12 (cf. fio's 513e37eeb01a3037763124869dfcdd6152d92ab8)
## - the --runtime CLI parameter is not taken into account
FIO_VERSION="$(${FIO_CMD} --version | \
                sed -e 's/^fio-//')"
if [ -z "${FIO_VERSION}" ]; then
    echo "E: unknown fio version."
    exit 1
else
    FIO_GLOBAL_CONFIG_FILE="$FIO_JOBFILES/global_config.fio.2-1"
    FIO_OUTPUT_FORMAT="json+"
    while IFS=. read FIO_VERSION_MAJOR FIO_VERSION_MIDDLE FIO_VERSION_MINOR; do
        if [ ${FIO_VERSION_MAJOR} -le 2 ]; then
            ([ ${FIO_VERSION_MAJOR} -eq 2 ] && \
             [ ${FIO_VERSION_MIDDLE} -lt 1 ]) && \
             FIO_GLOBAL_CONFIG_FILE="$FIO_JOBFILES/global_config.fio.2-0"
            ([ ${FIO_VERSION_MAJOR} -eq 2 ] && \
             [ ${FIO_VERSION_MIDDLE} -lt 12 ]) && \
             FIO_OUTPUT_FORMAT="json"
        fi
    done <<< $(echo ${FIO_VERSION})
fi
[ ${DEBUG} -eq 1 ] && \
 echo "D: FIO_VERSION: ${FIO_VERSION}"
 echo "D: FIO_GLOBAL_CONFIG_FILE: ${FIO_GLOBAL_CONFIG_FILE}"
 echo "D: FIO_OUTPUT_FORMAT: ${FIO_OUTPUT_FORMAT}"


export CLIENT_NAME=${CLIENT_NAME:-$HOSTNAME}
[ ${DEBUG} -eq 1 ] && \
 echo "D: CLIENT_NAME: ${CLIENT_NAME}"


prepare () {
  mkdir -p $SCENARIO_DIR
  mkdir -p $SCRATCH_DIR
  for F in $(ls -I "$SCENARIO_DIR/*.lock" $SCENARIO_DIR); do rm -rf $SCENARIO_DIR/$F; usleep 100000; done
}

syncpods () {
  BS_LOCK=$SCENARIO_DIR/${1}.lock
  mkdir -p $BS_LOCK
  while [ $(ls $BS_LOCK | wc -l) -lt $NUM_CLIENTS ]; do
    touch $BS_LOCK/$CLIENT_NAME
    usleep 100000
  done
  mkdir -p $CLIENT_DIR
}

cleanup () {
  sleep 1; rm -rf $SCENARIO_DIR/*.lock
  if [[ "${FIO_RW}" =~ "write" ]]; then rm -rf $SCRATCH_DIR; fi
  chown -R ${RESULT_USER:-1000}:${RESULT_GROUP:-1000} $CLIENT_DIR
}

SCRATCH_DIR=$DATA_PATH/fio_read && [[ "${FIO_RW}" =~ "write" ]] && SCRATCH_DIR=/data/fio_write/$CLIENT_NAME
export SCRATCH_DIR
[ ${DEBUG} -eq 1 ] && \
 echo "D: SCRATCH_DIR: ${SCRATCH_DIR}"
export SCENARIO_DIR=$RESULTS_PATH/$SCENARIO_NAME/$NUM_CLIENTS
[ ${DEBUG} -eq 1 ] && \
 echo "D: SCENARIO_DIR: ${SCENARIO_DIR}"
export CLIENT_DIR=$RESULTS_PATH/$SCENARIO_NAME/$NUM_CLIENTS/$CLIENT_NAME
[ ${DEBUG} -eq 1 ] && \
 echo "D: CLIENT_DIR: ${CLIENT_DIR}"


prepare
syncpods 0
let BS=128; let LIM=16*1024*1024
while [ $BS -le $LIM ]; do
  echo $BS
  ${FIO_CMD} ${FIO_GLOBAL_CONFIG_FILE} --fallocate=none --runtime=30 --directory=$SCRATCH_DIR --output-format=${FIO_OUTPUT_FORMAT} --blocksize=$BS --output=$CLIENT_DIR/${BS}.json
  syncpods $BS
  let BS=2*BS
done
cleanup
